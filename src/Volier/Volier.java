package Volier;

import Animals.Animal;
import java.io.Serializable;
import java.util.ArrayList;


public class Volier implements Serializable {
    String nazvanie;
    int count = 0;
    int i=0;

    public  ArrayList<Animal> listofAnimals = new ArrayList<>();

    public void takeAnimal(Animal animal) {
        count++;
        listofAnimals.add(animal);
    }

    public void getInfoAboutAnimal() {
        i=0;
        System.out.println("В " + nazvanie + " находятся следующие животные:");
        for(;i<listofAnimals.size();){
            System.out.println(i + "." + listofAnimals.get(i).getName());
            i++;
        }
    }


}
