package Animals;

import java.io.Serializable;

abstract public class Animal implements Serializable {
    private String name;
    private int weight, age;

    public abstract void Move();

    public Animal() {
    }

    public Animal(String name, int age, int weight) {
        this.name = name;
        this.age = age;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public int getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String toString(){
        return "Имя "+this.name+" Возраст "+this.age+" Вес "+this.weight;
    }

}
