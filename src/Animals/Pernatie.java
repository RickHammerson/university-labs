package Animals;

import Volier.Setka;

import java.io.Serializable;

public class Pernatie extends Animal implements Serializable {
    public Pernatie(String name, int age, int weight) {
        super(name, age, weight);
    }

    public static Setka setka = new Setka();

    public void Move() {
        setka.takeAnimal(this);
    }
}
