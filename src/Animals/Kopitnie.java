package Animals;

import Volier.Otkritiy;

import java.io.Serializable;

public class Kopitnie extends Animal implements Serializable {
    public Kopitnie(String name, int age, int weight) {
        super(name, age, weight);
    }

    public static Otkritiy otkritiy = new Otkritiy();

    public void Move() {
        otkritiy.takeAnimal(this);
    }
}
