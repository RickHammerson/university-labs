package Animals;

import Volier.InfrakrasnoeOsvechenie;

import java.io.Serializable;

public class Hladnokrovnie extends Animal implements Serializable {
    public Hladnokrovnie(String name, int age, int weight) {
        super(name, age, weight);
    }

    public static InfrakrasnoeOsvechenie infrakrasnoe = new InfrakrasnoeOsvechenie();

    public void Move() {
        infrakrasnoe.takeAnimal(this);
    }
}
