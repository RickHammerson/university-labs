import Animals.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;
import java.util.Scanner;


public class Main {
    LogFactory logs = new LogFactory();
    Scanner scanner = new Scanner(System.in);
    public static DB_Control db_control = new DB_Control();
    boolean exit=false;
    static ArrayList<Vodoplavayushie> arrayListOfAnimals = new ArrayList<>();
    static LinkedList<Vodoplavayushie> linkedListOfAnimals = new LinkedList<>();
    int countForArrayList=0;
    int countForLinkedList=0;
    int countOfDeletedElementsArrayList=0;
    int countOfDeletedElementsLinkedList=0;
    long startTime = 0;
    long endTime = 0;
    long difference = 0;
    long sumOfDif = 0;


    public static void main(String[] args) {
        Main main=new Main();
//        main.addRandomCountOfAnimalsInArrayList(10);
//        main.addRandomCountOfAnimalsInArrayList(100);
//        main.addRandomCountOfAnimalsInArrayList(1000);
//        main.addRandomCountOfAnimalsInArrayList(10000);
//        main.addRandomCountOfAnimalsInArrayList(100000);
        main.addRandomCountOfAnimalsInLinkedList(10);
        main.removeRandomCountOfAnimalsInLinkedList(1);
        main.addRandomCountOfAnimalsInLinkedList(100);
        main.removeRandomCountOfAnimalsInLinkedList(10);
//        main.addRandomCountOfAnimalsInLinkedList(1000);
//        main.addRandomCountOfAnimalsInLinkedList(10000);
//        main.addRandomCountOfAnimalsInLinkedList(100000);
        main.showAnimals();
        main.Menu();
        }

    public void addRandomCountOfAnimalsInArrayList(int countOfRandomNumbers) {
        Random random = new Random();
        sumOfDif = 0;
        for (int i=0;i<countOfRandomNumbers;i++) {
            startTime = 0;
            endTime = 0;
            difference = 0;
            Vodoplavayushie temp=new  Vodoplavayushie(returnRandomWord(),random.nextInt(20)+1,random.nextInt(50)+2);
            addAnimalInArrayList(temp);
        }
        logs.doLog("\n Count of add = "+countOfRandomNumbers +"\n Add total time = "+sumOfDif+"\n Add median time = "+sumOfDif/countOfRandomNumbers);
    }
    public void addRandomCountOfAnimalsInLinkedList(int countOfRandomNumbers) {
        Random random = new Random();
        sumOfDif = 0;
        for (int i=0;i<countOfRandomNumbers;i++) {
            startTime = 0;
            endTime = 0;
            difference = 0;
            Vodoplavayushie temp=new  Vodoplavayushie(returnRandomWord(),random.nextInt(20)+1,random.nextInt(50)+2);
            addAnimalInLinkedList(temp);
        }
        logs.doLog("\n Count of add = "+countOfRandomNumbers +"\n Add total time = "+sumOfDif+"\n Add median time = "+sumOfDif/countOfRandomNumbers);
    }

    public void addAnimalInArrayList(Vodoplavayushie animal) {
        countForArrayList++;
        startTime = System.nanoTime();
        arrayListOfAnimals.add(animal);
        endTime= System.nanoTime();
        difference = endTime - startTime;
        sumOfDif += difference;
        logs.doLog("Added element "+countForArrayList+" in ArrayList for "+difference);
    }
    public void addAnimalInLinkedList(Vodoplavayushie animal) {
        countForLinkedList++;
        startTime = System.nanoTime();
        linkedListOfAnimals.add(animal);
        endTime= System.nanoTime();
        difference = endTime - startTime;
        sumOfDif += difference;
        logs.doLog("Added element "+countForLinkedList+" in LinkedList for "+difference);
    }

    public void removeRandomCountOfAnimalsInArrayList(int countOfRandomNumbers) {
        sumOfDif = 0;
        for (int i=0;i<countOfRandomNumbers;i++) {
            startTime = 0;
            endTime = 0;
            difference = 0;
            removeAnimalsFromArrayList();
        }
        logs.doLog("\n Count of deleted = "+countOfRandomNumbers +"\n Add total time = "+sumOfDif+"\n Add median time = "+sumOfDif/countOfRandomNumbers);
    }
    public void removeRandomCountOfAnimalsInLinkedList(int countOfRandomNumbers) {
        sumOfDif = 0;
        for (int i=0;i<countOfRandomNumbers;i++) {
            startTime = 0;
            endTime = 0;
            difference = 0;
            removeAnimalsFromLinkedList();
        }
        logs.doLog("\n Count of deleted = "+countOfRandomNumbers +"\n Add total time = "+sumOfDif+"\n Add median time = "+sumOfDif/countOfRandomNumbers);
    }

    public void removeAnimalsFromArrayList() {
        Random random = new Random();
        int randomSelectedElement = random.nextInt(arrayListOfAnimals.size());
        countOfDeletedElementsArrayList++;
        startTime = System.nanoTime();
        arrayListOfAnimals.remove(randomSelectedElement);
        endTime= System.nanoTime();
        difference = endTime - startTime;
        sumOfDif += difference;
        logs.doLog("Removed element "+ randomSelectedElement+ " in ArrayList for "+difference);
    }

    public void removeAnimalsFromLinkedList() {
        Random random = new Random();
        int randomSelectedElement = random.nextInt(linkedListOfAnimals.size());
        countOfDeletedElementsArrayList++;
        startTime = System.nanoTime();
        linkedListOfAnimals.remove(randomSelectedElement);
        endTime= System.nanoTime();
        difference = endTime - startTime;
        sumOfDif += difference;
        logs.doLog("Removed element "+ randomSelectedElement+ " in LinkedList for "+difference);
    }

    public void showAnimals() {
        int i=1;
        for (Animal littleOne:linkedListOfAnimals){
            System.out.print(i+".");
            System.out.println(littleOne.toString());
            i++;
        }
    }



    public String returnRandomWord(){
        Random random = new Random();
        String vocabluary = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toLowerCase();
        int length = 10;
        char[] word = new char[length];
        for (int i=0;i<length;i++){
            word[i]=vocabluary.charAt(random.nextInt(vocabluary.length()));
        }
        return new String(word);
    }


    public void Menu(){

        while (!exit){
            System.out.println("Выберите пункт меню:\n" +
                    "1.Добавить новых животных\n" +
                    "2.Просмотреть вольеры\n" +
                    "3.Сохранить базу\n" +
                    "4.Загрузить базу\n" +
                    "5.Удалить базу\n" +
                    "6.Выход");
            logs.doLog("Выбор пункта меню");
            switch (scanner.nextInt()){
                case 1:
                    System.out.println("Введите количество животных,которых хотите поместить в вольеры");
                    int count=scanner.nextInt();
                    for (int i = 0; i < count; i++) {
                        System.out.println("Введите цифру,к какому виду относится животное\n 1.Водоплавающее\n 2.Пернатое\n 3.Копытное\n 4.Хладнокровное");
                        SdelatVibor(scanner.nextInt());
                    }
                    logs.doLog("Выбор количества животных для добавления");
                    break;
                case 2:
                    System.out.println("Какой вольер хотите посмотреть? \n " +
                            "1.Вольер-аквариум\n " +
                            "2.Вольер с сеткой\n " +
                            "3.Открытый вольер\n " +
                            "4.Вольер с инфракрасным освещением\n ");
                    SdelatViborVoliera(scanner.nextInt());
                    logs.doLog("Просмотр вольера");
                    break;
                case 3:
                    db_control.SaveDB();
                    logs.doLog("Сохранение ДБ");
                    break;
                case 4:
                    db_control.loadBBD();
                    logs.doLog("Загрузка ДБ");
                    break;
                case 5:
                    db_control.ClearDB();
                    logs.doLog("Очистка ДБ");
                    break;
                case 6:
                    exit=true;
                    logs.doLog("Выход из программы");
                    break;
                default:
                    System.out.println("Такого пункта меню нет");
                    logs.doLog("Выбор неверного пункта меню");
                    break;
            }
        }
    }

    public void SdelatVibor(int i) {
        Scanner scanner = new Scanner(System.in);
        switch (i) {
            case 1:
                System.out.println("Введите последовательно с новой строки название животного,его возраст и вес");
                new Vodoplavayushie(scanner.nextLine(),scanner.nextInt(),scanner.nextInt()).Move();
                logs.doLog("Добавление Водоплавающего животного");
                break;
            case 2:
                System.out.println("Введите последовательно с новой строки название животного,его возраст и вес");
                new Pernatie(scanner.nextLine(),scanner.nextInt(),scanner.nextInt()).Move();
                logs.doLog("Добавление Пернатого животного");
                break;
            case 3:
                System.out.println("Введите последовательно с новой строки название животного,его возраст и вес");
                new Kopitnie(scanner.nextLine(),scanner.nextInt(),scanner.nextInt()).Move();
                logs.doLog("Добавление Копытного животного");
                break;
            case 4:
                System.out.println("Введите последовательно с новой строки название животного,его возраст и вес");
                new Hladnokrovnie(scanner.nextLine(),scanner.nextInt(),scanner.nextInt()).Move();
                logs.doLog("Добавление Хладнокровного животного");
                break;
            default:
                System.out.println("Такого вида нет");
        }
    }

    public void SdelatViborVoliera(int i) {
        Scanner scanner = new Scanner(System.in);
        switch (i) {
            case 1:
                if (Vodoplavayushie.akvarium.listofAnimals.size()!=0){
                    System.out.println("Выберите цифру животного,о котором вы хотите узнать");
                    Vodoplavayushie.akvarium.getInfoAboutAnimal();
                    System.out.println("Выберите номер животного");
                    Animal inf=Vodoplavayushie.akvarium.listofAnimals.get(scanner.nextInt());
                    System.out.println(inf);
                } else System.out.println("В вольере нет животных");
                logs.doLog("Просмотр вольера водоплавающих");
                break;
            case 2:
                if (Pernatie.setka.listofAnimals.size()!=0){
                    System.out.println("Выберите цифру животного,о котором вы хотите узнать");
                    Pernatie.setka.getInfoAboutAnimal();
                    System.out.println("Выберите номер животного");
                    Animal inf=Pernatie.setka.listofAnimals.get(scanner.nextInt());
                    System.out.println(inf);
                } else System.out.println("В вольере нет животных");
                logs.doLog("Просмотр вольера с сеткой");

                break;
            case 3:
                if (Kopitnie.otkritiy.listofAnimals.size()!=0){
                    System.out.println("Выберите цифру животного,о котором вы хотите узнать");
                    Kopitnie.otkritiy.getInfoAboutAnimal();
                    System.out.println("Выберите номер животного");
                    Animal inf=Kopitnie.otkritiy.listofAnimals.get(scanner.nextInt());
                    System.out.println(inf);
                } else System.out.println("В вольере нет животных");
                logs.doLog("Просмотр открытого вольера");
                break;
            case 4:
                if (Hladnokrovnie.infrakrasnoe.listofAnimals.size()!=0){
                    System.out.println("Выберите цифру животного,о котором вы хотите узнать");
                    Hladnokrovnie.infrakrasnoe.getInfoAboutAnimal();
                    System.out.println("Выберите номер животного");
                    Animal inf=Hladnokrovnie.infrakrasnoe.listofAnimals.get(scanner.nextInt());
                    System.out.println(inf);
                } else System.out.println("В вольере нет животных");
                logs.doLog("Просмотр вольера с хладнокровными");

            default:
                System.out.println("Такого вольера нет");
                logs.doLog("Выбор неправильного вольера");
                break;
        }
    }


}
