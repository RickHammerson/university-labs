import java.io.*;
import java.util.Date;

public class LogFactory {

    BufferedInputStream bufferedInputStream;

    public LogFactory() {
        getLogin();
    }

    public void getLogin() {
        try {
            byte[] bytes = new byte[1000];
            System.out.print("Добро пожаловать - ");
            InputStream loginFile = new FileInputStream("src\\Login.txt");
            bufferedInputStream = new BufferedInputStream(loginFile, 1000);
            while ((bufferedInputStream.read(bytes)) != -1) {
            }
            String tt = new String(bytes, "UTF-8");
            System.out.print(tt);
            System.out.println();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bufferedInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void doLog(String logContext){
        Date date = new Date();
        try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("src\\Logs.txt",true))) {

                bufferedWriter.write(date.toString()+" "+logContext);
                bufferedWriter.newLine();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



}
