import Animals.*;
import Volier.Akvarium;
import Volier.InfrakrasnoeOsvechenie;
import Volier.Otkritiy;
import Volier.Setka;

import java.io.*;

public class DB_Control {
    public void SaveDB() {

        try (ObjectOutputStream objectOutputStreamAkvarium = new ObjectOutputStream(new FileOutputStream("src\\DB\\VolierAkvarium.dat"));
             ObjectOutputStream objectOutputStreamInfr = new ObjectOutputStream(new FileOutputStream("src\\DB\\VolierInfrakrasnoeOsvechenie.dat"));
             ObjectOutputStream objectOutputStreamOtkritiy = new ObjectOutputStream(new FileOutputStream("src\\DB\\VolierOtkritiy.dat"));
             ObjectOutputStream objectOutputStreamSetka = new ObjectOutputStream(new FileOutputStream("src\\DB\\VolierSetka.dat"))
        ) {
            if (Vodoplavayushie.akvarium!=null) objectOutputStreamAkvarium.writeObject(Vodoplavayushie.akvarium);

            if (Hladnokrovnie.infrakrasnoe!=null) objectOutputStreamInfr.writeObject(Hladnokrovnie.infrakrasnoe);

            if (Kopitnie.otkritiy!=null) objectOutputStreamOtkritiy.writeObject(Kopitnie.otkritiy);
            if (Pernatie.setka!=null) objectOutputStreamSetka.writeObject(Pernatie.setka);
            //    objectOutputStreamAkvarium.writeObject(Akvarium.listofAnimals);
//            objectOutputStreamInfr.writeObject(InfrakrasnoeOsvechenie.listofAnimals);
//            objectOutputStreamOtkritiy.writeObject(Otkritiy.listofAnimals);
//            objectOutputStreamSetka.writeObject(Setka.listofAnimals);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadBBD() {
        try (ObjectInputStream objectInputStreamAkvarium = new ObjectInputStream(new FileInputStream("src\\DB\\VolierAkvarium.dat"));
             ObjectInputStream objectInputStreamInfr = new ObjectInputStream(new FileInputStream("src\\DB\\VolierInfrakrasnoeOsvechenie.dat"));
             ObjectInputStream objectInputStreamOtkritiy = new ObjectInputStream(new FileInputStream("src\\DB\\VolierOtkritiy.dat"));
             ObjectInputStream objectInputStreamSetka = new ObjectInputStream(new FileInputStream("src\\DB\\VolierSetka.dat"))

        ) {
            Akvarium fromBDAkv = (Akvarium) objectInputStreamAkvarium.readObject();
            Vodoplavayushie.akvarium = fromBDAkv;
            InfrakrasnoeOsvechenie fromBDInfr = (InfrakrasnoeOsvechenie) objectInputStreamInfr.readObject();
            Hladnokrovnie.infrakrasnoe = fromBDInfr;
            Otkritiy fromBDOtkr = (Otkritiy) objectInputStreamOtkritiy.readObject();
            Kopitnie.otkritiy = fromBDOtkr;
            Setka fromBDSetka = (Setka) objectInputStreamSetka.readObject();
            Pernatie.setka = fromBDSetka;

//            Akvarium.listofAnimals = (ArrayList<Animal>) objectInputStreamAkvarium.readObject();
//            InfrakrasnoeOsvechenie.listofAnimals = (ArrayList<Animal>) objectInputStreamInfr.readObject();
//            Otkritiy.listofAnimals = (ArrayList<Animal>) objectInputStreamOtkritiy.readObject();
//            Setka.listofAnimals = (ArrayList<Animal>) objectInputStreamSetka.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void ClearDB() {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter("src\\DB\\VolierAkvarium.dat");

            writer.close();
            writer = new PrintWriter("src\\DB\\VolierInfrakrasnoeOsvechenie.dat");

            writer.close();
            writer = new PrintWriter("src\\DB\\VolierOtkritiy.dat");

            writer.close();
            writer = new PrintWriter("src\\DB\\VolierSetka.dat");

            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
